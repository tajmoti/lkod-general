export LKOD_MAIN_DOMAIN=lkod2.golemio.cz
export LKOD_SPARQL_DOMAIN=sparql2.golemio.cz

sed "s/<<MAIN_DOMAIN>>/$LKOD_MAIN_DOMAIN/" lkod-template.yaml > generated.yaml
sed -i.bak "s/<<SPARQL_DOMAIN>>/$LKOD_SPARQL_DOMAIN/" generated.yaml
