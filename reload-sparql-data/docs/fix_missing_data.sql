-- --------------------------------------------------------
SELECT
    d.id,
    ((d."data" ->> 'název') :: jsonb) ->> 'cs' as nazev,
    (((d."data" ->> 'klíčové_slovo') :: jsonb)) as klicova_slova,
    d."data" ->> 'periodicita_aktualizace' as periodicita_aktualizace,
    d."data" ->> 'prvek_rúian' as prvek_ruian,
    d."data",
    o.slug
FROM
    public.dataset d
    join public.organization o on o.id = d."organizationId"
where
    d.status = 'published'
    and o.slug not in ('operator-ict', 'mhp', 'ipr')
    and (
        (
            ((d."data" ->> 'klíčové_slovo') :: jsonb) -> 'cs'
        ) = '[]' :: jsonb
        or d."data" ->> 'periodicita_aktualizace' is null
        or (
            (d."data" ->> 'prvek_rúian') :: jsonb = '[]' :: jsonb
            or (d."data" ->> 'prvek_rúian') :: jsonb = '[""]' :: jsonb
        )
    )
order by
    o.slug,
    nazev;

-- --------------------------------------------------------
update
    public.dataset
set
    "data" = jsonb_set(
        "data",
        '{periodicita_aktualizace}',
        '"http://publications.europa.eu/resource/authority/frequency/CONT"'
    )
where
    "status" = 'published'
    and "organizationId" = 5
    and "data" ->> 'periodicita_aktualizace' is null;

-- --------------------------------------------------------
update
    public.dataset
set
    "data" = jsonb_set("data", '{klíčové_slovo,cs}', '["Praha"]')
where
    "status" = 'published'
    and "organizationId" not in (1, 2, 5)
    and (
        (("data" ->> 'klíčové_slovo') :: jsonb) -> 'cs'
    ) = '[]' :: jsonb;

-- --------------------------------------------------------
update
    public.dataset
set
    "data" = jsonb_set(
        "data",
        '{prvek_rúian}',
        '["https://vdp.cuzk.cz/vdp/ruian/vusc/19"]'
    )
where
    "status" = 'published'
    and "organizationId" not in (1, 2, 5)
    and (
        ("data" ->> 'prvek_rúian') :: jsonb = '[]' :: jsonb
        or ("data" ->> 'prvek_rúian') :: jsonb = '[""]' :: jsonb
    );

-- --------------------------------------------------------
update
    public.dataset
set
    "data" = jsonb_set(
        "data",
        '{distribuce,0,podmínky_užití}',
        '{"typ": "Specifikace podmínek užití","autor": {"cs": "Institut plánování a rozvoje hlavního města Prahy"},"osobní_údaje": "https://data.gov.cz/podmínky-užití/neobsahuje-osobní-údaje/","autor_databáze": {"cs": "Institut plánování a rozvoje hlavního města Prahy"},"autorské_dílo": "https://creativecommons.org/licenses/by/4.0/","databáze_jako_autorské_dílo": "https://creativecommons.org/licenses/by/4.0/","databáze_chráněná_zvláštními_právy": "https://data.gov.cz/podmínky-užití/není-chráněna-zvláštním-právem-pořizovatele-databáze/"}'
    )
where
    id = '';