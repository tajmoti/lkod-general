import axios from "axios";
import fs from "fs";
import dotenv from "dotenv";

dotenv.config();

const apiurl = process.env.APIURL;
const token = process.env.TOKEN;

const datasets = JSON.parse(fs.readFileSync("./datasets.json").toString());

for (const dataset of datasets) {
  const config = {
    method: "patch",
    url: `${apiurl}/datasets/${dataset}`,
    headers: {
      "Authorization": `Bearer ${token}`,
      "Content-Type": "application/json"
    },
  };

  console.log(`Dataset ${dataset}`);
  try {
    // unpublish
    await axios({
      ...config, data: JSON.stringify({
        "status": "unpublished"
      })
    });

    // publish
    await axios({
      ...config, data: JSON.stringify({
        "status": "published"
      })
    });

    console.log(`Dataset (${dataset}) was successfully republished`);
  } catch (err) {
    console.log(`Error while republishing dataset (${dataset})`);
    console.error(err.response.data);
  }
}
