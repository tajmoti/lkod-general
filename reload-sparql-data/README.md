# Reload SPARQL data

In case of delete all data from SPARQL, you can reload data by running the following commands:

## 1. Upload static vocabularies

Set ENV variables in lkod-backend to more frequent time, e.g. every 5 minutes:
```
SPARQL_VOCABULARY_CRONTIME="0 */5 * * * *"
SPARQL_ORGANIZATIONS_CRONTIME="0 */5 * * * *"
```

## 2. Get published datasets from DB

```sql
SELECT id
FROM public.dataset
WHERE status = 'published'
```

Result insert to json array in file `datasets.json` in this directory.

## 3. Set .env variables

- `APIURL`
  - api url of lkod-backend
- `TOKEN`
  - auth token of lkod-backend
  - you can get it by running following command:
    - `curl --location '<api url of lkod-backend>/auth/login' --header 'Content-Type: application/x-www-form-urlencoded' --data-urlencode 'email=user@email' --data-urlencode 'password=pass'`

## 4. Run script

```bash
npm install
npm run start
```

## 5. Check results

Go to lkod-catalog and check if all datasets are published and all static vocabularies are uploaded. If yes, you can set back ENV variables in lkod-backend to original values.


