# How to run this demo on localhost

## Prerequisites
- Docker
- docker-compose

## 1. Change current working directory

```bash
cd demo
cp -R initial_data/* data/
chmod -R 777 data/ftp_data
chown -R 1000:1000 data/fuseki-db/
```

## 2. Create secret and public keys
It's necessary for JWT token sign.

```bash
ssh-keygen -t rsa -b 4096 -m PEM -N '' -f data/lkod-demo.key
openssl rsa -in data/lkod-demo.key -pubout -outform PEM -out data/lkod-demo.key.pub
chmod 644 data/lkod-demo.key
chmod 644 data/lkod-demo.key.pub
```

Create FTP keys.
```bash
mkdir data/ftp_keys
openssl dhparam -out data/ftp_keys/pureftpd-dhparams.pem 2048
openssl req -x509 -nodes -newkey rsa:2048 -sha256 -days 1095 -subj "/C=US/ST=LKOD/L=LKOD" -keyout data/ftp_keys/pureftpd.pem -out data/ftp_keys/pureftpd.pem
```

## 3. Setup domains
Select either option `3. a.` or `3. b.` or `3. c.`

## 3. a. Using default localhost domains
Append line below to `/etc/hosts`.
```
127.0.0.1       lkod-catalog lkod-backend lkod-frontend lkod-sparql lkod-ftp-public
```

### 3. b. Run on your domain (EXPERIMENTAL)

#### Replace `YOUR_IP_OR_DOMAIN` by your IP or domain name

```bash
find ./ -type f -exec sed -i 's/lkod-catalog:3000/YOUR_IP_OR_DOMAIN/g' {} \;
find ./ -type f -exec sed -i 's/lkod-backend:3002/api.YOUR_IP_OR_DOMAIN/g' {} \;
find ./ -type f -exec sed -i 's/lkod-frontend:3001/admin.YOUR_IP_OR_DOMAIN/g' {} \;
find ./ -type f -exec sed -i 's/lkod-sparql:3030/sparql.YOUR_IP_OR_DOMAIN/g' {} \;
find ./ -type f -exec sed -i 's/lkod-ftp-public:3010/ftp.YOUR_IP_OR_DOMAIN/g' {} \;
```

#### Replace `YOUR_INTERNAL_IP` by your internal IP

```bash
find ./ -type f -exec sed -i 's/internal_ip/YOUR_INTERNAL_IP/g' {} \;
```

#### Choose your favourite reverse-proxy and run it

**Apache**
```bash
cd reverse-proxy/apache/
docker-compose up -d
```

**NGINX**
```bash
cd reverse-proxy/nginx/
docker-compose up -d
```

### 3. c. Run on your domain with HTTPS (EXPERIMENTAL)

#### Replace `YOUR_DOMAIN` by domain name (docker-compose)

```bash
find ./ -type f -exec sed -i 's/lkod-catalog:3000/YOUR_DOMAIN/g' {} \;
find ./ -type f -exec sed -i 's/lkod-backend:3002/api.YOUR_DOMAIN/g' {} \;
find ./ -type f -exec sed -i 's/lkod-frontend:3001/admin.YOUR_DOMAIN/g' {} \;
find ./ -type f -exec sed -i 's/lkod-sparql:3030/sparql.YOUR_DOMAIN/g' {} \;
find ./ -type f -exec sed -i 's/lkod-ftp-public:3010/ftp.YOUR_DOMAIN/g' {} \;
```

#### Replace `YOUR_INTERNAL_IP` by your internal IP (docker-compose,reverse proxy)

```bash
find ./ -type f -exec sed -i 's/internal_ip/YOUR_INTERNAL_IP/g' {} \;
```

#### Replace `YOUR_DOMAIN` by your domain (reverse proxy)

```bash
find ./ -type f -exec sed -i 's/your.domain/YOUR_DOMAIN/g' {} \;
```

#### Choose your favourite reverse-proxy and run it

**Apache**
In docker-compose.yaml change path_to_cert to path for your cert and key.
Make sure you rename both files to server.crt and server.key respectively.
```bash
cd reverse-proxy-https/apache/
docker compose up -d
```

**NGINX**
In docker-compose.yaml change path_to_cert to path for your cert and key.
Make sure you rename both files to server.crt and server.key respectively.
```bash
cd reverse-proxy-https/nginx/
docker compose up -d
```

## 4. Run docker compose
```bash
docker compose -f docker-compose-https.yaml up -d
```
Wait until all services are up.

## 5. Insert demo data
```bash
docker cp ./psql-demo-data.sql lkod-psql:/psql-demo-data.sql
docker exec lkod-psql bash -c "PGPASSWORD=pass psql -U postgres -d lkod -f /psql-demo-data.sql"
```

Wait until all static data are imported to SPARQL. It depends on the env `SPARQL_VOCABULARY_CRONTIME: "0 */5 * * * *"` of `lkod-backend` service.

## 6. Login to administration
Login on http://lkod-frontend:3001 (or http://admin.YOUR_IP_OR_DOMAIN)

```
user: test@golemio.cz
pass: pass
```

## 7. Insert testing dataset and publish it

Publish the existing test dataset or create the new dataset by the link to the NKOD form.

## 8. Enjoy the results

### Public catalog
http://lkod-catalog:3000/ (or http://YOUR_IP_OR_DOMAIN)

### Admin
http://lkod-frontend:3001/ (or http://admin.YOUR_IP_OR_DOMAIN)

Credentials see above.

### Admin API
http://lkod-backend:3002/ (or http://api.YOUR_IP_OR_DOMAIN)

List of public datasets in JSON format http://lkod-backend:3002/lod/catalog (or http://api.YOUR_IP_OR_DOMAIN/lod/catalog)

### Sparql endpoint
http://lkod-sparql:3030/ (or http://sparql.YOUR_IP_OR_DOMAIN)

Query is open. Login for updating data (user: lkod, pass: pass)

### FTP public access
http://lkod-ftp-public:3010/ (or http://ftp.YOUR_IP_OR_DOMAIN)

Public access to files on FTP. E.g. http://lkod-ftp-public:3010/lkod/operator-ict/b5ba0f4d-0298-43f4-b344-1bdbd3eb4073/test.csv (or http://ftp.YOUR_IP_OR_DOMAIN/lkod/operator-ict/b5ba0f4d-0298-43f4-b344-1bdbd3eb4073/test.csv)


## 9. (optional) Basic site customization

You can customize frontend sites (lkod-catalog and lkod-frontend) as you wish.

### 9. a. Customization

#### Clone repositories
```
git clone https://gitlab.com/operator-ict/golemio/lkod/lkod-catalog.git
```
or
```
git clone https://gitlab.com/operator-ict/golemio/lkod/lkod-frontend.git
```

#### Follow instructions
Each repository has file `docs/customization_cz.md` with instructions how to customize the site.

#### Build and run new docker image

##### Without own docker repository
Edit path to source code in the demo docker-compose.yaml
```bash
cd demo
nano docker-compose.yaml
```

and edit following lines
```yaml
...
  lkod-frontend:
    container_name: lkod-frontend
    # image: registry.gitlab.com/operator-ict/golemio/lkod/lkod-frontend/master:latest
    build: ../../lkod-frontend # path to cloned and edited lkod-frontend directory
    networks:
...

...
  lkod-catalog:
    container_name: lkod-catalog
    # image: registry.gitlab.com/operator-ict/golemio/lkod/lkod-catalog/master:latest
    build: ../../lkod-catalog # path to cloned and edited lkod-catalog directory
    networks:
...
```

##### With own docker repository
Edit docker images in the demo docker-compose.yaml
```bash
cd demo
nano docker-compose.yaml
```

and edit following lines
```yaml
...
  lkod-frontend:
    container_name: lkod-frontend
    image: link-to-your-lkod-frontend/master:latest
    networks:
...

...
  lkod-catalog:
    container_name: lkod-catalog
    image: link-to-your-lkod-catalog/master:latest
    networks:
...
```

### 9. b. Keep changes in your own repository

To keep your customize frontend sites up-to-date you can fork the git repositories and follow [this instructions](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) to update sites against upstream repositories.

An example of forked frontend site lkod-catalog is [LKOD Catalog benaktom](https://gitlab.com/benaktom/lkod-catalog-benaktom).
